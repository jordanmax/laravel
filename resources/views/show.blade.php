@extends('layouts.app')

@section('content')
    <div class="container">
        <div>View Ticket</div>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>Description</td>
                <td colspan="1">Action</td>
            </tr>
            </thead>
            <tbody>
            {{--@foreach($tickets as $ticket)--}}
            <tr>
                <td>{{$items->id}}</td>
                <td>{{$items->name}}</td>
                <td>{{$items->price}}</td>
                <td><a href="{{action('ItemController@index')}}" class="btn btn-primary">Back</a></td>
            </tr>
            {{--@endforeach--}}
            </tbody>
        </table>
        <div>
@endsection
