@extends('layouts.app')

@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <td>{{ trans('lang.ID') }}</td>
                <td>Name</td>
                <td>Price</td>
                <td colspan="3">Action</td>
            </tr>
            </thead>
            <tbody>
        @foreach ($items as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->price}}</td>
                <td><a href="{{action('ItemController@show',$item->id)}}" class="btn btn-primary">View</a></td>
                <td><a href="{{action('ItemController@edit',$item->id)}}" class="btn btn-primary">Edit</a></td>
                <td>
                    <form action="{{action('ItemController@destroy', $item->id)}}" method="post">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
            </tbody>
        </table>
</div>

@endsection
