@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Reg Data</td>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $ticket)
                <tr>
                    <td>{{$ticket->name}}</td>
                    <td>{{$ticket->email}}</td>
                    <td>{{$ticket->created_at}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$users->links()}}
        <div>
@endsection
