<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'HomeController@users')->name('users');
Route::get('/create/ticket','TicketController@create');
Route::post('/create/ticket','TicketController@store');
Route::get('/tickets', 'TicketController@index');
Route::get('/edit/ticket/{id}','TicketController@edit');
Route::patch('/edit/ticket/{id}','TicketController@update');
Route::delete('/delete/ticket/{id}','TicketController@destroy');

Route::get('/view/ticket/{id}','TicketController@view');
Route::get('ID/{id}',function($id){

    echo 'ID: '.$id;

});
Route::get('/user/{name?}',function($name = 'Virat Gandhi'){

    echo "Name: ".$name;

});
Route::get('localization/{locale}','LocalizationController@index');
Route::get('/validation','ValidationController@showform');
Route::post('/validation','ValidationController@validateform');
Route::resource('items', 'ItemController');
Route::get('/items/index', 'ItemController@index');
Route::get('role',[
    'middleware' => 'Role:editor',
    'uses' => 'TestController@index',
]);
Route::get('terminate',[
    'middleware' => 'terminate',
    'uses' => 'ABCController@index',
]);
Route::get('/cookie/set','CookieController@setCookie');
Route::get('/cookie/get','CookieController@getCookie');
Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');
Route::view('vue', 'vue');



