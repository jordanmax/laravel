<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'price', 'image'];

    public function saveTicket($data)
    {
        $this->name = $data['name'];
        $this->price = $data['price'];
        $this->avatar = $data['image'];
        $this->save();
        return 1;
    }

    public function updateTicket($data)
    {
        $item = $this->find($data['id']);
        $item->id = auth()->user()->id;
        $item->name = $data['title'];
        $item->price = $data['description'];
        $item->save();
        return 1;
    }
}
